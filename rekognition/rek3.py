import boto3
import os
import pprint

import cv2


red   = (255, 0, 0)
green = (0, 255, 0)
blue  = (0, 0, 255)
font = cv2.FONT_HERSHEY_SIMPLEX


rekognition_client = boto3.client('rekognition')

photo1 = os.path.join('fotos', 'Screen_Shot_2019_02_14_at_12.25.30_PM.0.jpg')
photo2 = os.path.join('fotos', 'unnamed.png')

photo = photo2

# -- haal de analyse op
with open(photo, 'rb') as image:

    response = rekognition_client.detect_labels(Image={'Bytes': image.read()})

# -- laad de foto in CV2
img = cv2.imread(photo, cv2.IMREAD_COLOR)
img_x, img_y, _ = img.shape
scale = min(800/img_x, 800/img_y)
print(scale)
print(int(img_x*scale), int(img_y*scale))

img = cv2.resize(img, (int(img_y*scale), int(img_x*scale))) #, interpolation='INTER_CUBIC')
img_x, img_y, _ = img.shape

print(img.shape)
print(img_x, img_y)

for label in response['Labels']:
    if label['Instances']:
        # print('label')
        pprint.pprint(label['Name'])
        label_name = label['Name']

        for instance in label['Instances']:
            print(instance)

            x_min = int(img_x*instance['BoundingBox']['Left'])
            x_max = int(img_x*instance['BoundingBox']['Width'] + x_min)
            y_min = int(img_y*instance['BoundingBox']['Top'])
            y_max = int(img_y*instance['BoundingBox']['Height'] + y_min)

            confidence  = instance['Confidence']

            if confidence > 80:

                # -- maak de rechthoek
                img = cv2.rectangle(img,
                                    (x_min, y_min),
                                    (x_max, y_max),
                                    (0, 255, 0),
                                    1)


                # -- tekst label
                tekst_label = f'{int(confidence)}% {label_name}'

                cv2.putText(img, tekst_label, (x_min, y_min - 5), font, 0.5, green, 1, cv2.LINE_AA)



cv2.imshow('image', img)
cv2.waitKey(0)
cv2.destroyAllWindows()